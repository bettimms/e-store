/**
 * Created by bettimms on 8/30/16.
 */

import {Subscription} from "rxjs";
import {ElementRef, Inject, Injectable, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {placeAdToolbarRowSelector} from "../../views/place-ad/models/grid-tile-item";

export class BaseRoute implements OnInit, OnDestroy {

  private sub: Subscription;

  constructor(private route: ActivatedRoute, public el: ElementRef, private elementSelector: any = placeAdToolbarRowSelector) {
  }

  ngOnInit() {
    // //Set title for pages
    this.sub = this.route
      .data
      .subscribe(v => {
        var pageTitleElement = this.el.nativeElement.ownerDocument.querySelector(this.elementSelector);
        let data: any = v;
        pageTitleElement.textContent = data.title;
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
