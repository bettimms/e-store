/**
 * Created by bettimms on 9/24/16.
 */

import {Component} from "@angular/core";

@Component({
  moduleId: module.id,
  selector: "es-footer",
  templateUrl: "footer.component.html",
  styleUrls: ["footer.component.scss"]
})

export class FooterComponent {
  folders = [
    {
      name: 'Photos',
      updated: new Date('1/1/16'),
    },
    {
      name: 'Recipes',
      updated: new Date('1/17/16'),
    },
    {
      name: 'Work',
      updated: new Date('1/28/16'),
    }
  ];
  notes = [
    {
      name: 'Vacation Itinerary',
      updated: new Date('2/20/16'),
    },
    {
      name: 'Kitchen Remodel',
      updated: new Date('1/18/16'),
    }
  ];

  //TODO Follow using this approach
  //https://github.com/angular/material2/issues/408

}
