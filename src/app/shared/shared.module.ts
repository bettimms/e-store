import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {AngularMaterialModule} from "./angular-material/angular-material.module";
import {MediaQueryStatus} from "./media-query-status";
import {DataProviderService} from "../views/place-ad/services/data-provider.service";
import {FooterComponent} from "./footer/footer.component";
import {HeaderComponent} from './header/header.component';
import {UserDrawerMenuService} from "../views/user/services/drawer-menu.service";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularMaterialModule
  ],
  exports: [CommonModule,
    FormsModule,
    RouterModule,
    AngularMaterialModule,
    MediaQueryStatus,
    FooterComponent,
    HeaderComponent
  ],
  declarations: [
    MediaQueryStatus,
    FooterComponent,
    HeaderComponent
  ],

})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [DataProviderService, UserDrawerMenuService]
    }
  }
}
