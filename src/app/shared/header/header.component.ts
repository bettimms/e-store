import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'es-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  @Input() sidenav:any;
  constructor() { }
  items = [
    {text: 'Phones',route:"phones"},
    {text: 'Tables',route:"tables"},
    {text: 'Wear',route:"wear"},
    {text: 'Tv',route:"tv"}
  ];

  listItems = [
    {name: 'Profile', route: "profile", icon: "person"},
    {name: 'Account', route: "account", icon: "account_circle"},
    {name: 'Log In', route: "login", icon: "fingerprint"},
    {name: 'Register', route: "register", icon: "person_add"},
    {name: 'Log out', route: "", icon: "exit_to_app"},
  ];

  ngOnInit() {
  }

}
