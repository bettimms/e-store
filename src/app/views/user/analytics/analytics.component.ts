/**
 * Created by bettimms on 9/3/16.
 */

import {Component, ElementRef} from "@angular/core";
import {userPageTitleSelector} from "../user.component";
import {ActivatedRoute} from "@angular/router";
import {BaseRoute} from "../../../shared/helpers/BaseRoute";
@Component({
  moduleId: module.id,
  templateUrl: "analytics.component.html",
  // styleUrls: ["../../user.component.css", "analytics.component.css"]
})

export class AnalyticsComponent extends  BaseRoute{

  constructor(route: ActivatedRoute, public el: ElementRef) {
    super(route, el, userPageTitleSelector);
  }
}
