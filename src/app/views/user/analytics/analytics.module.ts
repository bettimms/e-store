/**
 * Created by bettimms on 9/3/16.
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {AnalyticsComponent} from "./analytics.component";
import {AnalyticsDashboardComponent} from "./dashboard/dashboard.component";

@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [AnalyticsComponent, AnalyticsDashboardComponent],
  exports: [AnalyticsComponent]
})
export class AnalyticsModule {
}
