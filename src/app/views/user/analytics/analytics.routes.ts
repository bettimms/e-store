import {Route} from "@angular/router";
import {AnalyticsComponent} from "./analytics.component";
/**
 * Created by bettimms on 9/3/16.
 */

export const AnalyticsRoutes: Route[] = [
  {
    path: "analytics",
    component: AnalyticsComponent,
    data: {
      title: "Analytics"
    }
  }
];
