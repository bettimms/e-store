import {Route} from "@angular/router";
import {AdsComponent} from "./ads.component";
/**
 * Created by bettimms on 8/28/16.
 */

export const AdsRoutes: Route[] = [
  {
    path: "ads",
    component: AdsComponent,
    data:{
      title:"Ads"
    }
  }
];
