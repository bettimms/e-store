/**
 * Created by bettimms on 8/28/16.
 */

import {NgModule} from "@angular/core";
import {AdsComponent} from "./ads.component";
import {SharedModule} from "../../../shared/shared.module";

@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [AdsComponent],
  exports: [AdsComponent]
})
export class AdsModule {
}
