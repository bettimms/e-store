/**
 * Created by bettimms on 8/28/16.
 */

import {Component, ElementRef, ViewEncapsulation} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

// import any = jasmine.any;
import {BaseRoute} from "../../../shared/helpers/BaseRoute";
import {userPageTitleSelector} from "../user.component";

@Component({
  templateUrl: "ads.component.html",
  // styleUrls: ['../../user.component.css'],
  encapsulation: ViewEncapsulation.None,

})
export class AdsComponent extends BaseRoute {

  item: any = {};

  constructor(route: ActivatedRoute, public el: ElementRef) {
    super(route, el, userPageTitleSelector);
  }
}
