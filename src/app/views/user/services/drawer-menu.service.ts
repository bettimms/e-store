/**
 * Created by bettimms on 9/5/16.
 */

import {Injectable} from "@angular/core";
@Injectable()
export class UserDrawerMenuService {
  menuList: any[] = [{name: "Analytics", icon: "insert_chart", routerLink: "analytics"},
    {name: "Profile", icon: "person", routerLink: "profile"},
    {name: "Ads", icon: "star", routerLink: "ads"},
    {name: "Watchlist", icon: "bookmark", routerLink: "watchlist"},
    {name: "Account", icon: "account_circle", routerLink: "account"}
  ];
}
