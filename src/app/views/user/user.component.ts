import {Component, OnInit} from '@angular/core';
import {UserDrawerMenuService} from "./services/drawer-menu.service";

export const userPageTitleSelector: string = ".bs-user__page-title";


@Component({
  selector: 'es-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  menuList: any[];
  numbers: any[] = [];

  constructor(private drawerMenuService: UserDrawerMenuService) {
    this.menuList = drawerMenuService.menuList;

    for (var i = 0; i < 100; i++) {
      this.numbers.push(i);
    }
  }

  ngOnInit() {
  }
  // listItems = [
  //   {name: 'Profile', route: "profile", icon: "person"},
  //   {name: 'Account', route: "account", icon: "account_circle"},
  //   {name: 'Log In', route: "login", icon: "fingerprint"},
  //   {name: 'Register', route: "register", icon: "person_add"},
  //   {name: 'Log out', route: "", icon: "exit_to_app"},
  // ];

}
