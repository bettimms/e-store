/**
 * Created by bettimms on 8/28/16.
 */

import {Route} from "@angular/router";
import {WatchlistComponent} from "./watchlist.component";

export const WatchlistRoutes: Route[] = [
  {
    path: "watchlist",
    component: WatchlistComponent,
    data:{
      title:"Watch list"
    }
  }
];
