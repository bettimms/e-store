/**
 * Created by bettimms on 8/28/16.
 */

import {Component, ElementRef, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {BaseRoute} from "../../../shared/helpers/BaseRoute";
import {userPageTitleSelector} from "../user.component";


@Component({
  moduleId: module.id,
  templateUrl: "watchlist.component.html",
  // styleUrls: ['../../user.component.css']
})
export class WatchlistComponent extends BaseRoute {

  watchlist: string[] = ["iPhone 6s", "Samsung Note Pro", "Google Nexus 5"];

  constructor(route: ActivatedRoute, public el: ElementRef) {
    super(route, el, userPageTitleSelector);
  }
}
