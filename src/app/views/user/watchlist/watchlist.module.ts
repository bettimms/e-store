/**
 * Created by bettimms on 8/28/16.
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {WatchlistComponent} from "./watchlist.component";

@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [WatchlistComponent],
  exports: [WatchlistComponent]
})
export class WatchlistModule {
}
