/**
 * Created by bettimms on 8/28/16.
 */
import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {UserComponent} from "./user.component";
import {ProfileComponent} from "./profile/profile.component";
import {AdsComponent} from "./ads/ads.component";
import {WatchlistComponent} from "./watchlist/watchlist.component";
import {AccountComponent} from "./account/account.component";

@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [UserComponent, ProfileComponent, AdsComponent, WatchlistComponent, AccountComponent],
  exports: [UserComponent, ProfileComponent, AdsComponent, WatchlistComponent, AccountComponent]
})
export class UserModule {
}
