/**
 * Created by bettimms on 8/26/16.
 */

import {Component, ElementRef} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {BaseRoute} from "../../../shared/helpers/BaseRoute";
import {userPageTitleSelector} from "../user.component";


export interface UserProfileForm {
  key?: string;
  label: string;
  value: string;
}


@Component({
  moduleId: module.id,
  selector: 'es-profile',
  templateUrl: 'profile.component.html',
  // styleUrls: ['../../user.component.css', 'profile.component.css']
})

export class ProfileComponent extends BaseRoute {

  profileData: UserProfileForm[] = [
    {label: "Name", value: "Betim"},
    {label: "Gender", value: "M"},
    {label: "Nationality", value: "Kosovo"},
    {label: "Date of Birth", value: "14.09.1987"},
    {label: "Career Level", value: "Senior"},
    {label: "Current Location", value: "UAE"},
    {label: "Current Position", value: "Software Engineer"},
    {label: "Expected Salary", value: "12000-19000 AED"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Commitment", value: "Full time"},
    {label: "Notice Period", value: "2 months"}
  ];

  constructor(route: ActivatedRoute, public el: ElementRef) {
    super(route, el, userPageTitleSelector);
  }
}

