import {Route} from "@angular/router";
import {ProfileComponent} from "./profile.component";
/**
 * Created by bettimms on 8/26/16.
 */


export const ProfileRoutes: Route[] = [
  {
    path: "profile",
    component: ProfileComponent,
    data:{
      title:"Profile"
    }
  }
]
