/**
 * Created by bettimms on 8/26/16.
 */

import { NgModule } from '@angular/core';
import {ProfileComponent} from "./profile.component";
import {SharedModule} from "../../../shared/shared.module";


@NgModule({
  imports: [SharedModule.forRoot()],
  declarations:[ProfileComponent],
  exports:[ProfileComponent]
})
export class ProfileModule{}
