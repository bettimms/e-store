/**
 * Created by bettimms on 8/30/16.
 */

import {NgModule} from '@angular/core';
import {SharedModule} from "../../../shared/shared.module";
import {AccountComponent} from "./account.component";


@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [AccountComponent],
  exports: [AccountComponent]
})
export class AccountModule {
}
