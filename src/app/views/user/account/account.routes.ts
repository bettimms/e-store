/**
 * Created by bettimms on 8/30/16.
 */

import {Route} from "@angular/router";
import {AccountComponent} from "./account.component";

export const AccountRoutes: Route[] = [
  {
    path: "account",
    component: AccountComponent,
    data: {
      title: "Account"
    }
  }
]
