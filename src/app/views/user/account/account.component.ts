/**
 * Created by bettimms on 8/30/16.
 */

import {Component, ElementRef, ViewEncapsulation} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {BaseRoute} from "../../../shared/helpers/BaseRoute";
import {userPageTitleSelector} from "../user.component";


@Component({
  moduleId: module.id,
  selector: 'es-account',
  templateUrl: 'account.component.html',
  // styleUrls: ['../../user.component.css', 'account.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class AccountComponent extends BaseRoute {

  item: any = {};

  constructor(route: ActivatedRoute, public el: ElementRef) {
    super(route, el, userPageTitleSelector);
  }
}

