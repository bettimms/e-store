import {Route} from "@angular/router";
import {UserComponent} from "./user.component";
import {ProfileRoutes} from "./profile/profile.routes";
import {AdsRoutes} from "./ads/ads.routes";
import {WatchlistRoutes} from "./watchlist/watchlist.routes";
import {AccountRoutes} from "./account/account.routes";
import {AnalyticsRoutes} from "./analytics/analytics.routes";

/**
 * Created by bettimms on 8/28/16.
 */


const EmptyRoute: Route = {
  path: '',
  redirectTo: 'profile',
  pathMatch: 'full',
}

export const UserRoutes: Route[] = [

  {
    path: "",
    component: UserComponent,
    children: [
      EmptyRoute,
      ...ProfileRoutes,
      ...AdsRoutes,
      ...WatchlistRoutes,
      ...AccountRoutes,
      ...AnalyticsRoutes,
    ]
  }
];

// export const userRouting = RouterModule.forChild(UserRoutes);
