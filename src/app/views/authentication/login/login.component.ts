/**
 * Created by bettimms on 8/31/16.
 */

import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../services/authentication";
@Component({
  moduleId: module.id,
  templateUrl: "login.component.html",
  styleUrls: ["login.component.scss","../authentication.component.scss"]
})

export class LoginComponent {
  message: string;

  constructor(public authService: AuthService, public router: Router) {
  }

  setMessage() {
    this.message = 'Logged ' + (this.authService.isLoggedIn ? 'in' : 'out');
  }

  login() {
    this.message = 'Trying to log in ...';
    this.authService.login().subscribe(() => {
      this.setMessage();
      if (this.authService.isLoggedIn) {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/';
        // Redirect the user
        this.router.navigate([redirect]);
      }
    });
  }

  logout() {
    this.authService.logout();
    this.setMessage();
  }

}
