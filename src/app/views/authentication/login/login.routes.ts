/**
 * Created by bettimms on 8/31/16.
 */

import {Route} from "@angular/router";
import {LoginComponent} from "./login.component";
import {AuthGuard} from "../services/auth-guard.service";
import {AuthService} from "../services/authentication";
export const LoginRoutes: Route[] = [
  {
    path: "login",
    component: LoginComponent
  }
];

export const authProviders = [
  AuthGuard,
  AuthService
];
