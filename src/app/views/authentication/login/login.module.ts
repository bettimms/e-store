/**
 * Created by bettimms on 8/31/16.
 */


import {NgModule} from "@angular/core";
import {LoginComponent} from "./login.component";
import {SharedModule} from "../../../shared/shared.module";

@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [LoginComponent],
  exports: [LoginComponent]
})

export class LoginModule {
}
