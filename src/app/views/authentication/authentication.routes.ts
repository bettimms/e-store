/**
 * Created by bettimms on 8/31/16.
 */

import {Route, RouterModule} from "@angular/router";
import {authProviders, LoginRoutes} from "./login/login.routes";
import {AuthenticationComponent} from "./authentication.component";
import {RegisterRoutes} from "./register/register.routes";
import {ModuleWithProviders} from "@angular/core";
import {ResetPasswordRoutes} from "./reset-password/reset-password.routes";


const EmptyRoute: Route = {
  path: '',
  redirectTo: 'login',
  pathMatch: 'full',
}

export const AuthenticationRoutes: Route[] = [
  {
    path: "",
    component: AuthenticationComponent,
    children: [
      EmptyRoute,
      ...LoginRoutes,
      ...RegisterRoutes,
      ...ResetPasswordRoutes
    ]
  }
];

export const appRoutingProviders: any[] = [
  authProviders
];

export const authenticationRouting: ModuleWithProviders = RouterModule.forChild(AuthenticationRoutes);
