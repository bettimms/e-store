/**
 * Created by bettimms on 8/31/16.
 */

export function isLoggedin() {
  return !!localStorage.getItem('token');
}
