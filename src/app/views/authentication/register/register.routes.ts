/**
 * Created by bettimms on 8/31/16.
 */

import {Route} from "@angular/router";
import {RegisterComponent} from "./register.component";


export const RegisterRoutes: Route[] = [
  {
    path: "register",
    component: RegisterComponent
  }
];

