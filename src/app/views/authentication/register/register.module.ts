/**
 * Created by bettimms on 8/31/16.
 */


import {NgModule} from "@angular/core";
import {RegisterComponent} from "./register.component";
import {SharedModule} from "../../../shared/shared.module";

@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [RegisterComponent],
  exports: [RegisterComponent]
})

export class RegisterModule {
}
