/**
 * Created by bettimms on 8/31/16.
 */


import {NgModule} from "@angular/core";
import {AuthenticationComponent} from "./authentication.component";
import {appRoutingProviders, authenticationRouting} from "./authentication.routes";
import {ResetPasswordModule} from "./reset-password/reset-password.module";
import {LoginModule} from "./login/login.module";
import {RegisterModule} from "./register/register.module";
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [SharedModule.forRoot(),
    authenticationRouting,
    LoginModule,
    RegisterModule,
    ResetPasswordModule],
  declarations: [AuthenticationComponent],
  exports: [AuthenticationComponent],
  providers: [appRoutingProviders]
})

export class AuthenticationModule {
}
