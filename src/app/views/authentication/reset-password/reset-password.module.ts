/**
 * Created by bettimms on 8/31/16.
 */


import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {ResetPasswordComponent} from "./reset-password.component";

@NgModule({
  imports: [SharedModule.forRoot()],
  declarations: [ResetPasswordComponent],
  exports: [ResetPasswordComponent]
})

export class ResetPasswordModule {
}
