/**
 * Created by bettimms on 8/31/16.
 */

import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  templateUrl: "reset-password.component.html",
  styleUrls: ["reset-password.component.scss","../authentication.component.scss"]
})

export class ResetPasswordComponent {

  constructor(private router: Router) {

  }

  resetPassword() {
    this.router.navigate(["/login"]);
  }
}
