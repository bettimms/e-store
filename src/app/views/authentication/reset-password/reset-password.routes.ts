/**
 * Created by bettimms on 8/31/16.
 */

import {Route} from "@angular/router";
import {ResetPasswordComponent} from "./reset-password.component";


export const ResetPasswordRoutes: Route[] = [
  {
    path: "reset-password",
    component: ResetPasswordComponent
  }
];
