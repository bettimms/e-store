import { Component, OnInit } from '@angular/core';
import {UserProfileForm} from "../../../../user/profile/profile.component";

@Component({
  selector: 'es-item-description',
  templateUrl: './item-description.component.html',
  styleUrls: ['./item-description.component.scss']
})
export class ItemDescriptionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  profileData: UserProfileForm[] = [
    {label: "Brand", value: "Apple"},
    {label: "Memory Size", value: "16 GB"},
    {label: "Operating System", value: "iOS"},
    {label: "Battery Charge", value: "Built-in rechargeable lithium-ion"},
    {label: "Warranty", value: "Yes"},
    {label: "Condition", value: "Brand New"}
  ];
}
