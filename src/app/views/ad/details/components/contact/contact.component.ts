import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'es-item-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  lat: number = 25.2048;
  lng: number = 55.2708;
  constructor() { }

  ngOnInit() {
  }

}
