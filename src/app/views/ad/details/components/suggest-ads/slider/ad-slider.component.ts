/**
 * Created by bettimms on 9/19/16.
 */


import {Component, Input} from "@angular/core";
@Component({
  moduleId: module.id,
  selector: "es-ad-slider",
  templateUrl: "ad-slider.component.html",
  styleUrls: ["ad-slider.component.scss"]
})

export class AdSliderComponent {
  @Input() title: string;
  @Input() image: string;
  @Input() price: string;
  @Input() currency: string;
}
