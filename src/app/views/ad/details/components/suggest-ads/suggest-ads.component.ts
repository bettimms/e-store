import { Component, OnInit } from '@angular/core';
import {SwiperConfigInterface} from "ngx-swiper-wrapper";

@Component({
  selector: 'es-suggest-ads',
  templateUrl: './suggest-ads.component.html',
  styleUrls: ['./suggest-ads.component.scss']
})
export class SuggestAdsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  items: any[] = [
    {
      title: "iPhone 6S - 16GB, 4G LTE, Space Gray",
      image: "assets/images/iPhone-6-and-iPhone-6-Plus2.png",
      price: "2359.00",
      currency: "AED"
    },
    {
      title: "Samsung S7 - 32GB, 4G LTE, Space Gray",
      image: "assets/images/nexus9-on.jpg",
      price: "1659.50",
      currency: "AED"
    },
    {
      title: "Samsung Nexus 6 - 32GB, 4G LTE, Space Gray",
      image: "assets/images/nexus6-on.jpg",
      price: "3900.39",
      currency: "AED"
    },
    {
      title: "Samsung Nexus 6 - 32GB, 4G LTE, Space Gray",
      image: "/assets/images/Iphone-7-Concept-HD-Wallpapers-full-6.jpg",
      price: "999.00",
      currency: "AED"
    },
    {
      title: "Samsung Nexus 6 - 32GB, 4G LTE, Space Gray",
      image: "assets/images/iphone-png-23.png",
      price: "2330.45",
      currency: "AED"
    },
    {
      title: "Samsung Nexus 6 - 32GB, 4G LTE, Space Gray",
      image: "assets/images/devices.jpg",
      price: "2959.90",
      currency: "AED"
    },
    {
      title: "Samsung Nexus 6 - 32GB, 4G LTE, Space Gray",
      image: "assets/images/wear-black-on.png",
      price: "3359.99",
      currency: "AED"
    },
  ]

  public config: SwiperConfigInterface = {
    scrollbar: null,
    nested:false,
    // width:1000,
    direction: 'horizontal',
    slidesPerView: 7,
    // scrollbarHide: true,
    keyboardControl: true,
    // mousewheelControl: true,
    scrollbarDraggable: false,
    scrollbarSnapOnRelease: false,
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    // autoplay:5000,
    // speed:1000,
    // centeredSlides: true,
    spaceBetween: 10,
    breakpoints: {
      1824:{
        slidesPerView:6,
        spaceBetween:10
      },
      1460:{
        slidesPerView:5,
        spaceBetween:10
      },
      1224:{
        slidesPerView:4,
        spaceBetween:10
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      // 640: {
      //   slidesPerView: 1,
      //   spaceBetween: 10
      // },
      400: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  };


}
