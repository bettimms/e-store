import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestAdsComponent } from './suggest-ads.component';

describe('SuggestAdsComponent', () => {
  let component: SuggestAdsComponent;
  let fixture: ComponentFixture<SuggestAdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggestAdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
