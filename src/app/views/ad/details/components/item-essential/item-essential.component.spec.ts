import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEssentialComponent } from './item-essential.component';

describe('ItemEssentialComponent', () => {
  let component: ItemEssentialComponent;
  let fixture: ComponentFixture<ItemEssentialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemEssentialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEssentialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
