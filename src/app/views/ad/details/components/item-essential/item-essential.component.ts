import {Component, OnInit} from '@angular/core';
import {UserProfileForm} from "../../../../user/profile/profile.component";
import {SwiperConfigInterface} from "ngx-swiper-wrapper";

@Component({
  selector: 'es-item-essential',
  templateUrl: './item-essential.component.html',
  styleUrls: ['./item-essential.component.scss']
})
export class ItemEssentialComponent implements OnInit {
  url: string = "../../../assets/images/";

  constructor() {
  }

  ngOnInit() {
  }

  items = [{
    name: "iPhone 4",
    route: "iphone-4",
    image: this.url + "iPhone-6-and-iPhone-6-Plus2.png"
  }, {name: "iPhone 4S", route: "iphone-4s", image: this.url + "Iphone-7-Concept-HD-Wallpapers-full-6.jpg"},
    {name: "iPhone 5", route: "iphone-5", image: this.url + "dashboard.png"}, {
      name: "iPhone 5S",
      route: "iphone-5s",
      image: this.url + "dashboard.png"
    },
  ]
  shareItems = [
    {name: "Facebook", icon: "facebook"},
    {name: "Twitter", icon: "twitter"},
    {name: "Email", icon: "email"},
  ]

  profileData: UserProfileForm[] = [
    {label: "Brand", value: "Apple"},
    {label: "Memory Size", value: "16 GB"},
    {label: "Operating System", value: "iOS"},
    {label: "Battery Charge", value: "Built-in rechargeable lithium-ion"},
    {label: "Warranty", value: "Yes"},
    {label: "Condition", value: "Brand New"}
  ];

  config: SwiperConfigInterface = {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    grabCursor:true,
    slideToClickedSlide: true,
    keyboardControl:true
  }

  thumbs: SwiperConfigInterface = {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 'auto',
    // touchRatio: 0.2,
    slideToClickedSlide: true,
    grabCursor:true,

  }
}
