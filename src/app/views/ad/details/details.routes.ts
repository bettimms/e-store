import {Route} from "@angular/router";
import {DetailsComponent} from "./details.component";

export const AdDetailsRoutes: Route[] = [{
  path: "details/:name",
  component: DetailsComponent
}]
