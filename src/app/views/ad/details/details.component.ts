import {Component, OnInit} from '@angular/core';
import {UserProfileForm} from "../../user/profile/profile.component";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor() {
  }

  conditions: any = [{value: "Brand new"},
    {value: "0-1 month"},
    {value: "1-6 months"},
    {value: "1 year"},
    {value: "Older"}];

  ngOnInit() {
  }


}
