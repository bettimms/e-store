import {Route} from "@angular/router";
import {AdComponent} from "./ad.component";
import {AdDetailsRoutes} from "./details/details.routes";

const EmptyRoute:Route = {
  path:"",
  redirectTo:"/",
  pathMatch:"full"
}

export const AdRoutes: Route[] = [
  {
    path:"ad",
    component:AdComponent,
    children:[
      EmptyRoute,
      ...AdDetailsRoutes
    ]
  }
]
