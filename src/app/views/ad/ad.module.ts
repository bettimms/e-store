import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {AdComponent} from "./ad.component";
import { DetailsComponent } from './details/details.component';
import { ItemEssentialComponent } from './details/components/item-essential/item-essential.component';
import {ItemDescriptionComponent} from "./details/components/item-description/item-description.component";
import { ContactComponent } from './details/components/contact/contact.component';
import { AgmCoreModule } from '@agm/core';
import { SuggestAdsComponent } from './details/components/suggest-ads/suggest-ads.component';
import {SwiperConfigInterface, SwiperModule} from "ngx-swiper-wrapper";
import {AdSliderComponent} from "./details/components/suggest-ads/slider/ad-slider.component";


const SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
  keyboardControl: true
};

@NgModule({
  imports: [SharedModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDbNlpGDOpU_xeelFwuzAdP2bUeMkis3Y4'
    }),
    SwiperModule.forRoot(SWIPER_CONFIG)
  ],
  declarations:[AdComponent, DetailsComponent,
    ItemEssentialComponent,
    ItemDescriptionComponent,
    ContactComponent,
    SuggestAdsComponent,
    AdSliderComponent
  ]
})
export class AdModule {
}
