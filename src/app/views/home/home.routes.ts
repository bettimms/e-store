import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home.component";
/**
 * Created by bettimms on 7/21/17.
 */
export const HomeRoutes: Routes = [{
  path: "",
  component: HomeComponent
}];
