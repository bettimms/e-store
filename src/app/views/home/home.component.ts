import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'est-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }
  tiles = [1,2,3,4];
  errorMessage: string;
  url:string = "../../../assets/images/"
  items = [{name: "iPhone 4", route: "iphone-4", image:this.url+"iPhone-6-and-iPhone-6-Plus2.png"}, {name: "iPhone 4S", route: "iphone-4s",image:this.url+"Iphone-7-Concept-HD-Wallpapers-full-6.jpg"},
    {name: "iPhone 5", route: "iphone-5", image:this.url+"dashboard.png"}, {name: "iPhone 5S", route: "iphone-5s",image:this.url+"dashboard.png"},
    {name: "iPhone 6", route: "iphone-6",image:this.url+"dashboard.png"}, {name: "iPhone 6S", route: "iphone-6s",image:this.url+"dashboard.png"}]

  models: any[] = [
    { name: "Derek", status: "Teaching children to read", about: "I'm a model", messages: [], rows: 1, cols:1,image:this.url+"dashboard.png"},
    { name: "Hansel", status: "Kitesurfing", about: "I'm a model", messages: [], rows: 1, cols:1,image:this.url+"Iphone-7-Concept-HD-Wallpapers-full-6.jpg" },
    { name: "Valentina", status: "Kitesurfing", about: "I'm a model", messages: [], rows: 1 , cols:1,image:this.url+"dashboard.png"},
    { name: "Mugatu", status: "Designing the masterplan", about: "I'm a tirant", messages: [], rows: 3, cols:1,image:this.url+"dashboard.png"},
    { name: "Katinka", status: "Teaching children to read", about: "I'm a model", messages: [], rows: 1, cols:1,image:this.url+"iPhone-6-and-iPhone-6-Plus2.png"},
    { name: "Rufus", status: "Kitesurfing", about: "I'm a model", messages: [], rows: 1 , cols:1,image:this.url+"Iphone-7-Concept-HD-Wallpapers-full-6.jpg"},
    { name: "Brint", status: "Teaching children to read", about: "I'm a model", messages: [], rows: 1, cols:1,image:this.url+"iPhone-6-and-iPhone-6-Plus2.png"},
    { name: "Meekus", status: "Teaching children to read", about: "I'm a model", messages: [], rows: 1, cols:1,image:this.url+"iPhone-6-and-iPhone-6-Plus2.png"},
    { name: "EvilDJ", status: "Teaching children to read", about: "I'm a model", messages: [], rows: 1, cols:1,image:this.url+"iPhone-6-and-iPhone-6-Plus2.png"},
    { name: "JPPrewit", status: "Teaching children to read", about: "I'm a model", messages: [], rows: 1, cols:1,image:this.url+"iPhone-6-and-iPhone-6-Plus2.png"},
  ];

}
