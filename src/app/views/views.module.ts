import {NgModule} from "@angular/core";
import {HomeComponent} from "./home/home.component";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {AboutComponent} from './about/about.component';
import {NewAdModule} from "./place-ad/place-ad.module";
import {UserModule} from "./user/user.module";
import {AuthenticationModule} from "./authentication/authentication.module";
import {AnalyticsModule} from "./user/analytics/analytics.module";
import { AdComponent } from './ad/ad.component';
import {AdModule} from "./ad/ad.module";

/**
 * Created by bettimms on 7/21/17.
 */
@NgModule({
  imports: [FormsModule, SharedModule.forRoot(),
    NewAdModule,
    UserModule,
    AnalyticsModule,
    AdModule
  ],
  declarations: [HomeComponent, AboutComponent],
  exports: []
})
export class ViewsModule {
}
