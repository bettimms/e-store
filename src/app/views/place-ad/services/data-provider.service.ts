import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {GridTileItem} from "../models/grid-tile-item";
/**
 * Created by bettimms on 8/21/16.
 */



const applePhones = [{name: "iPhone 4", route:"iphone-4"}, {name: "iPhone 4S",route:"iphone-4s"},
  {name: "iPhone 5",route:"iphone-5"}, {name: "iPhone 5S",route:"iphone-5s"},
  {name: "iPhone 6",route:"iphone-6"}, {name: "iPhone 6S",route:"iphone-6s"}]
const googlePhones = [{name: "Nexus 2"}, {name: "Nexus 3"}, {name: "Nexus 4"}, {name: "Nexus 5"}, {name: "Nexus 6"}]
const samsungPhones = [{name: "Samsung S1"}, {name: "Samsung S3"}, {name: "Samsung S4"}, {name: "Samsung S5"}, {name: "Samsung S6"},
  {name: "Samsung Galaxy Note 3"}, {name: "Samsung Galaxy Note 4"}, {name: "Samsung Galaxy Note 5"}
  , {name: "Samsung Galaxy Note 6"}, {name: "Samsung Galaxy Note 7"}]
const cubotPhones = [{name: "Cubot VIII"}, {name: "Cubot IX"}, {name: "Cubot X"}, {name: "Cubot XI"}]
const htsPhones = [{name: "HTC Sensation"}, {name: "HTC One"}, {name: "HTC X"}]



const subcategories: GridTileItem[] = [{
  name: "Accer", tileColor: "#4DD0E1", icon: "phone_iphone", selected: false,
  models: []
},
  {name: "Alcatel", tileColor: "#00BFA5", icon: "phone_iphone", selected: false, models: []},
  {name: "Apple", tileColor: "#FF4081", icon: "phone_iphone", selected: false, models: applePhones},
  {name: "Asus", tileColor: "#DCE775", icon: "phone_iphone", selected: false, models: []},
  {name: "Blackburry", tileColor: "#9575CD", icon: "phone_iphone", selected: false, models: []},
  {name: "Google", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: googlePhones},
  {name: "HTC", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models:htsPhones},
  {name: "Huawei", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: []},
  {name: "LG", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: []},
  {name: "Lenovo", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: []},
  {name: "Nokia", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: []},
  {name: "Motorola", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: []},
  {name: "Samsung", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models:samsungPhones},
  {name: "Sony", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: []},
  {name: "Cubot", tileColor: "#90A4AE", icon: "phone_iphone", selected: false, models: cubotPhones},
];



@Injectable()
export class DataProviderService {
  get(): GridTileItem[] {
    return subcategories.sort((a, b)=> {
      return a.name.localeCompare(b.name);
    });
  }

  getByCategory(categoryName:string):any[]{
    return subcategories.filter(category=>category.name.toLowerCase() === categoryName)
      .map(item=>item.models);
  }

}
