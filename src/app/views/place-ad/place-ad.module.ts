import {NgModule} from "@angular/core";
import {PlaceAdComponent} from "./place-ad.component";
import {CategoryComponent} from "./category/category.component";
import {SharedModule} from "../../shared/shared.module";
import {SubCategoryComponent} from "./subcategory/subcategory.component";
import {ItemDetailsComponent} from "./item-details/item-details.component";
import {VendorComponent} from "./vendor/vendor.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [FormsModule, SharedModule.forRoot()],
  declarations: [PlaceAdComponent,
    CategoryComponent,
    SubCategoryComponent,
    ItemDetailsComponent,
    VendorComponent]
})
export class NewAdModule {
}
