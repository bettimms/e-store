import {
  AfterViewInit, Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild,
  ViewChildren
} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {GridTileItem, placeAdToolbarRowSelector} from "../models/grid-tile-item";
import {DataProviderService} from "../services/data-provider.service";

/**
 * Created by bettimms on 8/21/16.
 */


@Component({
  templateUrl: "subcategory.component.html"
})

export class SubCategoryComponent implements OnInit, OnDestroy {

  private sub: Subscription;
  subcategories: GridTileItem[];
  subcategoryName: string;

  constructor(private subcategoryService: DataProviderService, private router: Router,
              private route: ActivatedRoute,private el: ElementRef) {
    this.subcategories = this.subcategoryService.get();
  }


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.subcategoryName = params['name'];
      var toolbar = this.el.nativeElement.ownerDocument.querySelector(placeAdToolbarRowSelector);
      toolbar.textContent = this.subcategoryName.charAt(0).toUpperCase() + this.subcategoryName.slice(1);
      // this.subcategories = this.subcategoryService.getByCategory(subCategoryName);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onSelect(item: GridTileItem) {
    item.selected = !item.selected;
    this.router.navigate([`place-ad/category/${this.subcategoryName.toLowerCase()}`, item.name.toLowerCase()]);
  }
}
