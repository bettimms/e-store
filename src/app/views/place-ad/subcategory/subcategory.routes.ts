/**
 * Created by bettimms on 8/27/16.
 */

import {Route} from "@angular/router";
import {SubCategoryComponent} from "./subcategory.component";

export const SubcategoryRoutes: Route[] = [
  {
    path: 'category/:name',
    component: SubCategoryComponent,
    data: {
      title: 'Subcategory'
    }
  }
];
