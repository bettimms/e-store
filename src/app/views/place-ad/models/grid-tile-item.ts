/**
 * Created by bettimms on 8/21/16.
 */

export const placeAdToolbarRowSelector:string = "md-toolbar.new-item-header";


export interface GridTileItem{
  name:string;
  icon:string;
  tileColor:string;
  selected:boolean;
  models?:any[];
}
