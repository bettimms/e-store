import { Component, OnInit } from '@angular/core';
import {GridTileItem} from "../models/grid-tile-item";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  categories: GridTileItem[] = [{name: "Phones", tileColor: "#4DD0E1", icon: "phone_iphone", selected: false},
    {name: "Tablets", tileColor: "#00BFA5", icon: "tablet_mac", selected: false},
    {name: "Laptops", tileColor: "#FF4081", icon: "laptop", selected: false},
    {name: "Computers", tileColor: "#DCE775", icon: "desktop_mac", selected: false},
    {name: "Monitors", tileColor: "#9575CD", icon: "tv", selected: false},
    {name: "Watches", tileColor: "#90A4AE", icon: "watch", selected: false}];

}
