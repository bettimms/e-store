import {Routes} from "@angular/router";
import {CategoryComponent} from "./category.component";

export const CategoryRoutes: Routes = [
  {
    path: "category",
    component: CategoryComponent,
    data: {
      title: 'Category'
    }
  }
]
