import {Route} from "@angular/router";
import {VendorComponent} from "./vendor.component";
/**
 * Created by bettimms on 8/27/16.
 */

export const VendorRoutes:Route[] = [
  {
    path: 'category/:name/:vendor',
    component: VendorComponent,
    data: {
      title: 'Vendors',
    }
  }
];
