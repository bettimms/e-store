/**
 * Created by bettimms on 8/21/16.
 */

import {Component, OnInit, OnDestroy, ElementRef} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {DataProviderService} from "../services/data-provider.service";
import {placeAdToolbarRowSelector} from "../models/grid-tile-item";

@Component({
  templateUrl: "vendor.component.html",
  styleUrls:["../place-ad.component.scss"]
})

export class VendorComponent implements OnInit,OnDestroy{
  private sub: Subscription;
  devices:any[];
  stringFilter: string = '';
  constructor(private subcategoryService:DataProviderService, private route: ActivatedRoute,private el:ElementRef) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let vendorName = params['vendor'];
      var toolbar = this.el.nativeElement.ownerDocument.querySelector(placeAdToolbarRowSelector);
      toolbar.textContent = vendorName.charAt(0).toUpperCase()+vendorName.slice(1);
      this.devices = this.subcategoryService.getByCategory(vendorName)[0];
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
