import {Route, Routes} from "@angular/router";
import {PlaceAdComponent} from "./place-ad.component";
import {CategoryRoutes} from "./category/category.routes";
import {SubcategoryRoutes} from "./subcategory/subcategory.routes";
import {VendorRoutes} from "./vendor/vendor.routes";
import {ItemDetailsRoutes} from "./item-details/item-details.routes";

export const EmptyRoute:Route = {
  path:"",
  redirectTo:"category",
  pathMatch:"full"
}

export const NewAdRoutes:Routes = [{
  path:"place-ad",
  component:PlaceAdComponent,
  children:[
    EmptyRoute,
    ...CategoryRoutes,
    ...SubcategoryRoutes,
    ...VendorRoutes,
    ...ItemDetailsRoutes
  ]

}];
