import {Route} from "@angular/router";
import {ItemDetailsComponent} from "./item-details.component";
/**
 * Created by bettimms on 8/27/16.
 */

export const ItemDetailsRoutes: Route[] = [
  {
    path: 'category/:name/:vendor/:model',
    component: ItemDetailsComponent,
    data: {
      title: 'Item details'
    }
  }
];
