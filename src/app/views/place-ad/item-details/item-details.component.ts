/**
 * Created by bettimms on 8/22/16.
 */


import {Component, ElementRef} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {BaseRoute} from "../../../shared/helpers/BaseRoute";
import {placeAdToolbarRowSelector} from "../models/grid-tile-item";

@Component({
  moduleId: module.id,
  templateUrl: "item-details.component.html",
  styleUrls: ["item-details.component.scss", "../place-ad.component.scss"]
})

export class ItemDetailsComponent extends BaseRoute {


  conditions: any = [{value: "Brand new"},
    {value: "0-1 month"},
    {value: "1-6 months"},
    {value: "1 year"},
    {value: "Older"}];
  currencyPrefix: string = "AED";

  constructor(route: ActivatedRoute,public el: ElementRef) {
    super(route, el);
  }
}
