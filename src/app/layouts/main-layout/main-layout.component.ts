import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'es-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  views: Object[] = [
    {
      name: "Phones",
      description: "All about phones",
      icon: "smartphone",
      route:"phones"
    },
    {
      name: "Tables",
      description: "New tablets!",
      icon: "tablet",
      route:"tables"
    },{
      name: "Wear",
      description: "Watches here!",
      icon: "watch",
      route:"wear"
    },
    {
      name: "TV",
      description: "Smart TVs!",
      icon: "tv",
      route:"tv"
    }
  ];

}
