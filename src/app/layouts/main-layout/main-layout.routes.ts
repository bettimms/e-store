import {RouterModule, Routes} from "@angular/router";
import {MainLayoutComponent} from "./main-layout.component";
import {HomeRoutes} from "../../views/home/home.routes";
import {AboutRoutes} from "../../views/about/about.routes";
import {NewAdRoutes} from "../../views/place-ad/place-ad.routes";
import {UserRoutes} from "../../views/user/user.routes";
import {AdRoutes} from "../../views/ad/ad.routes";

/**
 * Created by bettimms on 7/21/17.
 */

export const MainLayoutRoutes: Routes = [{
  path: "",
  component: MainLayoutComponent,
  children: [
    ...HomeRoutes,
    ...AboutRoutes,
    ...NewAdRoutes,
    ...UserRoutes,
    ...AdRoutes
  ]
}];
export const mainLayoutRoutes = RouterModule.forChild(MainLayoutRoutes);
