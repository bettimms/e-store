/**
 * Created by bettimms on 7/21/17.
 */
import {NgModule} from "@angular/core";
import {MainLayoutComponent} from "./main-layout.component";
import {mainLayoutRoutes} from "./main-layout.routes";
import {ViewsModule} from "../../views/views.module";
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [SharedModule.forRoot(),mainLayoutRoutes, ViewsModule],
  exports: []
})
export class MainLayoutModule {
}
