import {Route, RouterModule, Routes} from "@angular/router";
import {AnalyticsRoutes} from "../../views/user/analytics/analytics.routes";
import {UserRoutes} from "../../views/user/user.routes";
import {AccountRoutes} from "../../views/user/account/account.routes";
/**
 * Created by bettimms on 7/21/17.
 */

export const EmptyLayoutRoutes: Route[] = [
  ...AccountRoutes
];

export const emptyLayoutRouting = RouterModule.forChild(EmptyLayoutRoutes);
