/**
 * Created by bettimms on 7/21/17.
 */
import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {emptyLayoutRouting} from "./empty-layout.routes";
import {UserModule} from "../../views/user/user.module";
import {AnalyticsModule} from "../../views/user/analytics/analytics.module";
import {AuthenticationModule} from "../../views/authentication/authentication.module";

@NgModule({
  imports: [SharedModule.forRoot(), emptyLayoutRouting,AuthenticationModule],
  declarations: [],
  exports: []
})
export class EmptyLayoutModule {
}
