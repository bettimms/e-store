import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {EmptyLayoutModule} from "./layouts/empty-layout/empty-layout.module";
import {MainLayoutModule} from "./layouts/main-layout/main-layout.module";
import {routing} from "./app.routes";
import {RouterModule} from "@angular/router";
import {SharedModule} from "./shared/shared.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    SharedModule.forRoot(),
    MainLayoutModule,
    EmptyLayoutModule
  ],
  exports:[RouterModule,AppComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
